<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package high
 */

get_header();
?>

<section id="title-page">
	<div class="container">
		<div class="col-xs-12">
			<h4>EMPREENDIMENTOS</h4>
			<h1>Confira nossos destaques</h1>
		</div>
	</div>
</section>

<section class="miolo list-empreendimento-section">
	<div class="container">
		<div class="col-xs-12">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-2',
            ) );

            echo do_shortcode('[ajax_load_more container_type="ul" css_classes="lista-empreendimento list" post_type="empreendimento" posts_per_page="6" taxonomy="categoria" taxonomy_terms="" taxonomy_operator="IN" scroll="false" button_label="Mais empreendimentos" archive="true" no_results_text="<h3>Desculpe, ainda não temos nenhum empreendimento cadastrado nessa categoria.</h3>" button_done_label="&nbsp"]'); ?>
		</div>
	</div>
</section>


<?php
//get_sidebar();
get_footer();
