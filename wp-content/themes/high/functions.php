<?php
/**
 * high functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package high
 */

if ( ! function_exists( 'high_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function high_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on high, use a find and replace
		 * to change 'high' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'high', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'high' ),
            'menu-2' => esc_html__( 'Rodapé', 'high' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'high_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'high_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function high_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'high_content_width', 640 );
}
add_action( 'after_setup_theme', 'high_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function high_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'high' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'high' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'high_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function high_scripts() {
	wp_enqueue_style( 'high-style', get_stylesheet_uri() );

	wp_enqueue_script( 'high-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'high-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'high_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}









///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////ADAPTAÇOES JUCAMILLO

//CLASSE PARENT E CLASSE NOME NO BODY
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );
add_filter('body_class','body_class_section');
function body_class_section($classes) {
    global $wpdb, $post;
    if (is_page()) {
        if ($post->post_parent) {
            $parent  = end(get_post_ancestors($current_page_id));
        } else {
            $parent = $post->ID;
        }
        $post_data = get_post($parent, ARRAY_A);
        $classes[] = 'parent-' . $post_data['post_name'];
    }
    return $classes;
}



//BREADCRUMBS

function custom_breadcrumbs() {
       
    // Settings
    $separator          = '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" class="svg-inline--fa fa-angle-right fa-w-6 fa-3x"><path fill="currentColor" d="M187.8 264.5L41 412.5c-4.7 4.7-12.3 4.7-17 0L4.2 392.7c-4.7-4.7-4.7-12.3 0-17L122.7 256 4.2 136.3c-4.7-4.7-4.7-12.3 0-17L24 99.5c4.7-4.7 12.3-4.7 17 0l146.8 148c4.7 4.7 4.7 12.3 0 17z" class=""></path></svg>';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            } else{

              
                echo '<li><a href="' . get_home_url() . '/blog" title="Blog">Blog</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
                echo '<li><a href="' . get_home_url() . '/blog" title="Blog">Blog</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Resultado de busca por:' . get_search_query() . '">Resultado de busca por: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}
add_shortcode( 'custom_breadcrumbs', 'custom_breadcrumbs' );




//PAGINAÇÃO

function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}



//FUNCAO IF IS POST TYPE
function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) return true;
    return false;
}




//REMOVER 'TITULOS' DOS ARCHIVES

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});


//CRIAR OPTIONS MENU ACF PRO
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Informações Gerais');
}



//PAGINACAO CUSTOM
function custom_pagination($numpages = '', $pagerange = '', $paged='') {if (empty($pagerange)) {$pagerange = 2;}/**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */global $paged;if (empty($paged)) {$paged = 1;}if ($numpages == '') {global $wp_query;$numpages = $wp_query->max_num_pages;if(!$numpages) {$numpages = 1;}}/** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */$pagination_args = array(
            'base'=> get_pagenum_link(1) . '%_%',
            'format'=> 'page/%#%',
            'total'=> $numpages,
            'current'=> $paged,
            'show_all'=> False,
            'end_size'=> 1,
            'mid_size'=> $pagerange,
            'prev_next'=> True,
            'prev_text'=> __('«'),
            'next_text'=> __('»'),
            'type'=> 'plain',
            'add_args'=> false,
            'add_fragment'=> '');

   $paginate_links = paginate_links($pagination_args);
   if ($paginate_links) {echo "<div class='full-pag'><nav class='custom-pagination'>";echo $paginate_links;echo "</nav></div>";}}













//SHORTCODE POSTS RELACIONADOS
function my_related_posts() {
     $args = array('posts_per_page' => 3, 'post_in'  => get_the_tag_list(),
 'post__not_in' => array($post->ID),);
     $the_query = new WP_Query( $args );

    $string .= '<ul class="noticias list owl-carousel">';

     while ( $the_query->have_posts() ) : $the_query->the_post();

                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name ); 
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    $string .= '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    $string .= '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.short_title('...', 5).'</a></h3>
                                <p>'.strip_tags( get_the_excerpt() ).'</p>
                                <div class="hover-btn">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                    </svg>
                                    <span>
                                    Ler matéria completa
                                    </span>
                                </a></div>
                            </div>
                            </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'my_related_posts', 'my_related_posts' );




//REDIRECIONAMENTO DE SINGLE POSTS LISTAGEM
add_action( 'template_redirect', 'subscription_redirect_post' );

function subscription_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  /*if ( is_single() && 'boleto' ==  $queried_post_type ) {
    wp_redirect( home_url( 'boletos', 'relative' ), 301 );
    exit;
  }*/
  if ( is_single() && 'banner' ==  $queried_post_type ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}


function short_title($after = '', $length) {
    $mytitle = explode(' ', get_the_title(), $length);
    if (count($mytitle)>=$length) {
        array_pop($mytitle);
        $mytitle = implode(" ",$mytitle). $after;
    } else {
        $mytitle = implode(" ",$mytitle);
    }
    return $mytitle;
}



function ultimas_novidades($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 2, 
        'orderby'=>'date',
        'order'=>'desc',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => 'destaque',
            ),
        ),
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="noticias owl-carousel">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name ); 
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    $string .= '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    $string .= '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.short_title('...', 5).'</a></h3>
                                <p>'.strip_tags( get_the_excerpt() ).'</p>
                                <div class="hover-btn">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                    </svg>
                                    <span>
                                    Ler matéria completa
                                    </span>
                                </a></div>
                            </div>
                            </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'ultimas_novidades', 'ultimas_novidades' );







function destaque_noticia($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 1, 
        'orderby'=>'date',
        'order'=>'desc'
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="noticias destaque_noticia">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name ); 
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    $string .= '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    $string .= '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.short_title('...', 5).'</a></h3>
                                <p>'.strip_tags( get_the_excerpt() ).'</p>
                                <div class="hover-btn">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                    </svg>
                                    <span>
                                    Ler matéria completa
                                    </span>
                                </a></div>
                            </div>
                            </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'destaque_noticia', 'destaque_noticia' );

add_filter( 'excerpt_length', function($length) {
    return 20;
} );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


function prefix_limit_post_types_in_search( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array( 'post' ) );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'prefix_limit_post_types_in_search' );






//SHORTCODE POSTS RELACIONADOS
function mais_lidos($atts) {

    $bannerArgs = array( 
        'post_type' => 'post', 
        'posts_per_page' => 4, 
        'meta_key' => 'wpb_post_views_count', 
        'orderby' => 'meta_value_num', 
        'order' => 'DESC' 
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="noticias popular">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li><div class="block-img">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a></div>
                                <div class="info">
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name ); 
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    $string .= '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    $string .= '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
                                </div>
                            </li>';
    endwhile;
            $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'mais_lidos', 'mais_lidos' );











function destaque_empreendimento($atts) {

    $bannerArgs = array( 
        'post_type' => 'empreendimento', 
        'posts_per_page' => -1, 
        'orderby'=>'date',
        'order'=>'desc',
        'tax_query' => array(
            array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms' => 'destaque',
            ),
        ),
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="empreendimento dest-owl-emp owl-carousel">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li><div class="container">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <div class="txt">';
                                    if(get_field('texto_menor')):
                                        $string .= '<h4>'.get_field('texto_menor').'</h4>';
                                    else:
                                        $string .= '<h4>'.get_the_title().'</h4>';
                                    endif;
                                    if(get_field('texto_maior')):
                                        $string .= get_field('texto_maior');
                                    endif;
                                    $string .= '<a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn"><span>';
                                    if(get_field('texto_botao')):
                                        $string .= get_field('texto_botao');
                                    else:
                                        $string .= 'Ver empreendimento';
                                    endif;
                                    $string .= '</span><svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg></a>';
                    $string .= '</div></div>';
                    $string .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-img">
                                    <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');"></a>
                                </div></div>
                        </li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'destaque_empreendimento', 'destaque_empreendimento' );











function ultimos_empreendimentos($atts) {

    $bannerArgs = array( 
        'post_type' => 'empreendimento', 
        'posts_per_page' => -1, 
        'orderby'=>'date',
        'order'=>'desc',
        'tax_query' => array(
            array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms' => 'entregue',
            ),
        ),
    );
    $bannerLoop = new WP_Query( $bannerArgs ); 
    $string .= '<ul class="lista-empreendimento owl-emp owl-carousel">';
    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');"></a>

                                <div class="info">';

                                    $terms = get_the_terms( $post->ID, 'categoria' );
                                        if($terms){
                                            $string .= '<h4>';
                                            foreach ($terms as $term) {
                                                if($term->slug != 'destaque'){
                                                $string .= '<a href="'.get_home_url().'/empreendimentos/categoria/'.$term->slug.'" title="'.$term->name.'">'.$term->name.'</a>';

                                                }
                                            }
                                            $string .= '</h4>';
                                        }
                                    $string .= '<h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
                                                <div class="btn-full">
                                                    <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                                    </svg>
                                                    <span>
                                                    Ver empreendimento
                                                    </span>
                                                    </a>
                                                </div>
                                        </div>';
                    $string .= '</li>';
    endwhile;
    $string .= '</ul>';
    wp_reset_postdata();
    return $string;
}
add_shortcode( 'ultimos_empreendimentos', 'ultimos_empreendimentos' );








function galeria_topo($atts) {

    $images = get_field('galeria_topo');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-full owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="gallery">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'galeria_topo', 'galeria_topo' );




function fotos($atts) {

    $images = get_field('fotos');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-galery2 owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="fotos">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'fotos', 'fotos' );


function perspectivas($atts) {

    $images = get_field('perspectivas');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-galery2 owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="perspectivas">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'perspectivas', 'perspectivas' );


function plantas($atts) {

    $images = get_field('plantas');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-galery2 owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="plantas">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'plantas', 'plantas' );


function obra($atts) {

    $images = get_field('obra');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-galery2 owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="obra">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'obra', 'obra' );


function local($atts) {

    $images = get_field('local');
    if( $images ): 
        $string .= '<ul class="owl-galery owl-galery2 owl-carousel">';
            foreach( $images as $image ):
                $string .= '<li>
                    <a href="'.esc_url($image['url']).'" data-caption="'.esc_html($image['caption']).'"  data-fancybox="local">
                         <img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" />
                    </a>
                </li>';
            endforeach;
        $string .= '</ul>';
    endif; 

    return $string;
}
add_shortcode( 'local', 'local' );







function videos($atts) {

    if( have_rows('videos') ):
    $string .= '<ul class="owl-galery owl-video owl-videos owl-carousel"> ';
        while( have_rows('videos') ): the_row();
        $string .= '<li>
                        <div class="video-responsive">
                        '.get_sub_field('video').'
                        </div>
                    </li>';
        endwhile;
    $string .= '</ul>';
    endif; 
    return $string;
}
add_shortcode( 'videos', 'videos' );







function caracteristicas($atts) {

    if( have_rows('carac') ):
    $string .= '<ul class="list-carac"> ';
        while( have_rows('carac') ): the_row();
        $string .= '<li>
                        '.get_sub_field('icon').'
                        <span>'.get_sub_field('nome').'</span>
                    </li>';
        endwhile;
    $string .= '</ul>';
    endif; 
    return $string;
}
add_shortcode( 'caracteristicas', 'caracteristicas' );





function andamento_obra($atts) {


    if( have_rows('passos') ):
    $string .= '<ul class="passos"> ';
        while( have_rows('passos') ): the_row();
        $string .= '<li class="ac'.get_sub_field('ativo').'">
                        '.get_sub_field('nome').'
                    </li>';
        endwhile;
    $string .= '</ul>';
    endif; 
    return $string;

    return $string;
}
add_shortcode( 'andamento_obra', 'andamento_obra' );


