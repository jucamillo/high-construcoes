<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package high
 */

get_header();
?>

<section id="title-page">
	<div class="container not-search">
		<div class="col-xs-12 col-lg-8 col-md-7">
			<h4>NOTÍCIAS</h4>
			<h1>Novidades da HIGH</h1>
		</div>
		<div class="col-lg-4 col-md-5 col-xs-12 search-title">
			<?php get_search_form(); ?>
		</div>
	</div>
</section>

<section class="miolo list-blog-section">
	<div class="container">
		<div class="col-xs-12 search-mobile">
			
			<?php get_search_form(); ?>
		</div>
		<div class="col-lg-8 col-md-7 col-xs-12">
			<?php echo do_shortcode('[destaque_noticia]'); ?>
			
		</div>
		<div class="col-lg-4 col-md-5 col-xs-12">
			<h3 class="pop">Mais populares</h3>
			<?php echo do_shortcode('[mais_lidos]'); ?>
		</div>
		<div class="col-xs-12 ultimas-noticias-col">
			<h3>
				Últimas notícias
			</h3>
			<?php
			if ( have_posts() ) : ?>
				<ul class="noticias list">
					

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile; ?>


				</ul>
				<?php
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>


<script type="text/javascript">

if( jQuery(window).width() < 992 ){
	jQuery('ul.noticias.popular').addClass('owl-carousel');
	jQuery('ul.noticias.popular').owlCarousel({
	    margin:0,
	    responsiveClass:true,
	    dots: true,
	    nav:false,
	    autoHeight:false,
	    autoplay: false,
	    autoplayTimeout: 10000,
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:1,
	            margin: 0
	        },
	        992:{
	            items:1,
	            margin: 0
	        },
	        1200:{
	            items:1,
	            margin: 0
	        }
	    }
	})	
};


</script>

<?php
get_footer();
