<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package high
 */

?>

<section class="no-results not-found">
	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'high' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>
			<br>
			<p>Desculpe, mas não encontramos nenhum resultado para a sua busca. Tente novamente com outro(s) termo(s).</p>
			<?php

		else :
			?>

			<h3>Parece que não conseguimos encontrar o que está procurando. Tente uma busca!</h3>
			<?php

		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
