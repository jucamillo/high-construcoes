<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package high
 */
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if ( !is_singular() ) :


                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                echo '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">
                                </a>
                                <div class="info">
                                <ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name );
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    echo '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    echo '</ul>
                                <h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.short_title('...', 5).'</a></h3>
                                <p>'.strip_tags( get_the_excerpt() ).'</p>
                                <div class="hover-btn">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                    </svg>
                                    <span>
                                    Ler matéria completa
                                    </span>
                                </a></div>
                            </div>
                            </li>';
         endif; ?>