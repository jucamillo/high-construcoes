<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package high
 */

?>
	</section>

	<footer id="colophon" class="site-footer">
			<div class="container">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <ul class="contato">
                        <?php if( get_field('endereco', 'option') ): ?>
                            <li>
                                <address>
                                    <svg width="13" height="15" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 8.49478C7.604 8.49478 8.5 7.59934 8.5 6.49606C8.5 5.39278 7.604 4.49738 6.5 4.49738C5.396 4.49738 4.5 5.39278 4.5 6.49606C4.5 7.59934 5.396 8.49478 6.5 8.49478Z" stroke="white" stroke-linecap="square"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 6.49606C12.5 11.4928 7.5 14.4909 6.5 14.4909C5.5 14.4909 0.5 11.4928 0.5 6.49606C0.5 3.18522 3.187 0.5 6.5 0.5C9.813 0.5 12.5 3.18522 12.5 6.49606Z" stroke="white" stroke-linecap="square"/>
                                    </svg>

                                    <span><?php the_field('endereco', 'option'); ?></span>
                                </address>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('telefone', 'option') ): ?>
                            <li>
                                <a href="tel:<?php the_field('telefone', 'option'); ?>" target="_blank">
                                    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.71922 0.5H2.5C1.39543 0.5 0.5 1.39543 0.5 2.5V4.5C0.5 10.0228 4.97715 14.5 10.5 14.5H12.5C13.6046 14.5 14.5 13.6046 14.5 12.5V11.118C14.5 10.7393 14.286 10.393 13.9472 10.2236L11.5313 9.01564C10.987 8.74349 10.3278 9.01652 10.1354 9.59384L9.83762 10.4871C9.64474 11.0658 9.05118 11.4102 8.45309 11.2906C6.05929 10.8119 4.18814 8.94071 3.70938 6.54691C3.58976 5.94882 3.93422 5.35526 4.51286 5.16238L5.62149 4.79284C6.11721 4.6276 6.40214 4.10855 6.2754 3.60162L5.68937 1.25746C5.57807 0.812297 5.17809 0.5 4.71922 0.5Z" stroke="white"/>
                                    </svg>
                                    <span><?php the_field('telefone', 'option'); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if( get_field('email', 'option') ): ?>
                            <li>
                                <a href="mailto:<?php the_field('email', 'option'); ?>" target="_blank">
                                    <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.5 4L7.5 8L14.5 4M1.5 1H13.5C14.0523 1 14.5 1.44772 14.5 2V12C14.5 12.5523 14.0523 13 13.5 13H1.5C0.947716 13 0.5 12.5523 0.5 12V2C0.5 1.44772 0.947715 1 1.5 1Z" stroke="white"/>
                                    </svg>

                                    <span><?php the_field('email', 'option'); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>


                </div>
                <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-2',
                    ) );
                    ?>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">

                    <?php if( have_rows('redes_sociais', 'option') ): ?>
                        <ul class="social">  
                        <?php while( have_rows('redes_sociais', 'option') ): the_row(); ?>
                            <li class="social">
                                <a href="<?php the_sub_field('link'); ?>" target="_blank">
                                    <?php the_sub_field('icone'); ?>
                                </a>
                            </li>
                        <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>


                <div class="full-footer col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php if( get_field('copyrights', 'option') ): ?>
                            <?php the_field('copyrights', 'option'); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php if( get_field('qualitare', 'option') ): ?>
                            <a href="http://qualitare.com/" class="sign" target="_blank">
                                <img src="<?php the_field('qualitare', 'option'); ?>">
                            </a>
                            
                        <?php endif; ?>
                        
                    </div>
                </div>
			</div>
	</footer>
</div>

<?php wp_footer(); ?>

<script type="text/javascript">
	

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });


    jQuery('.vc_btn3-container > .vc_btn3.vc_btn3-color-danger').append('<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>');

    jQuery('.vc_btn3-container > .vc_btn3.vc_btn3-color-white').prepend('<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>');

    jQuery('button.alm-load-more-btn.btn').append('<svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 1L9 9L1 0.999999" stroke="#F9F9F9"/></svg>');


    jQuery(document).delegate("#sobre #ctas .vc_col-sm-3", "click", function() {
       window.location = jQuery(this).find("a").attr("href");
    });






    jQuery(document).delegate("header nav.main-navigation .menu-toggle", "click", function() {
       jQuery(".menu-mobile").addClass("active");
    });
    jQuery(document).delegate(".menu-mobile .menu-toggle", "click", function() {
       jQuery(".menu-mobile").removeClass("active");
    });
});


jQuery('.dest-owl-emp').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:true,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 10000,
    navText: ['<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 17L1 9L9 1" stroke="#050709"/></svg>', '<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#050709"/></svg>'],
    responsive:{
        0:{
            items:1
        },
        768:{
            items:1
        },
        992:{
            items:1
        },
        1200:{
            items:1
        }
    }
})



jQuery('body.home .noticias').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 10000,
    responsive:{
        0:{
            items:1,
            margin: 0
        },
        768:{
            items:1,
            margin: 0
        },
        992:{
            items:2,
            margin: 20
        },
        1200:{
            items:2,
            margin: 30
        }
    }
})

jQuery('.owl-emp').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 10000,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2,
            margin: 20
        },
        992:{
            items:2,
            margin: 20
        },
        1200:{
            items:3,
            margin: 30
        }
    }
})






if( jQuery(window).width() < 768 ){

    jQuery('#sobre #ctas .vc_col-sm-3:first-child').remove()

    jQuery('#sobre #ctas .vc_col-sm-3').removeClass('up1');
    jQuery('#sobre #ctas .vc_col-sm-3').removeClass('up2');
    jQuery('#sobre #ctas .vc_col-sm-3').removeClass('up3');
    jQuery('#sobre #ctas').addClass('owl-carousel');
    jQuery('#sobre #ctas').owlCarousel({
        margin:0,
        responsiveClass:true,
        dots: true,
        nav:false,
        autoHeight:false,
        autoplay: false,
        autoplayTimeout: 10000,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:1,
                margin: 0,
                stagePadding:0,
            },
            768:{
                items:3,
                margin: 0
            },
            992:{
                items:3,
                margin: 0
            },
            1200:{
                items:3,
                margin: 0
            }
        }
    })



    jQuery('section#acreditamos > * > .vc_col-sm-6:last-child > * > .wpb_wrapper ').addClass('owl-carousel');
    jQuery('section#acreditamos > * > .vc_col-sm-6:last-child > * > .wpb_wrapper ').owlCarousel({
        margin:20,
        responsiveClass:true,
        dots: true,
        nav:false,
        autoHeight:false,
        autoplay: false,
        autoplayTimeout: 10000,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:1,
            },
            768:{
                items:1,
                margin: 0
            },
            992:{
                items:1,
                margin: 0
            },
            1200:{
                items:1,
                margin: 0
            }
        }
    })



    jQuery(document).delegate(".menu-categorias-container ul li.current-menu-item > a", "click", function(event) {
        event.preventDefault();
       jQuery('.menu-categorias-container ul').toggleClass('open');
    });

    jQuery('ul.passos li.ac').remove()

    jQuery(document).delegate(".vc_tta.vc_general ul.vc_tta-tabs-list li.vc_tta-tab.vc_active > a", "click", function(event) {
        event.preventDefault();
       jQuery('.vc_tta.vc_general ul.vc_tta-tabs-list').toggleClass('open');
    });



    jQuery(document).delegate(".vc_tta.vc_general ul.vc_tta-tabs-list li.vc_tta-tab:not(.vc_active) > a", "click", function(event) {
        //event.preventDefault();
       jQuery('.vc_tta.vc_general ul.vc_tta-tabs-list').removeClass('open');
    });



};




var left = {
    delay: 300,
    distance: '100px',
    duration: 1300,
    origin: 'left',
    easing: 'ease-in-out',
};


var right = {
    delay: 300,
    distance: '100px',
    duration: 1300,
    origin: 'right',
    easing: 'ease-in-out',
};





var up1 = {
    delay: 100,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up2 = {
    delay: 400,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up3 = {
    delay: 700,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};




var down = {
    delay: 100,
    distance: '100px',
    duration: 1000,
    origin: 'top',
    easing: 'ease-in-out',
};




var fade = {
    delay: 100,
    distance: '100px',
    duration: 1000,
    origin: 'bottom',
    easing: 'ease-in-out',
};





jQuery(document).ready(function(){



    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up2', up2);
    ScrollReveal().reveal('.up3', up3);
    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.left', left);
    ScrollReveal().reveal('.right', right);
    ScrollReveal().reveal('.down', down);
    ScrollReveal().reveal('.fade', fade);





});


            var maskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

            jQuery('input[name="fone"]').mask(maskBehavior, options);




</script>
</body>
</html>
