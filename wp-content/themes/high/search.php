<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package high
 */

get_header();
?>



<section id="title-page">
	<div class="container not-search">
		<div class="col-xs-12 col-lg-8 col-md-7">
			<h4>NOTÍCIAS</h4>
			<h1>Resultado de busca</h1>
		</div>
		<div class="col-lg-4 col-md-5 col-xs-12 search-title">
			<?php get_search_form(); ?>
		</div>
	</div>
</section>

<section class="miolo list-blog-section">
	<div class="container">
		<div class="col-xs-12 search-mobile">
			
			<?php get_search_form(); ?>
		</div>
		<div class="col-xs-12">
			<h3>

					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Resultado de busca por: %s', 'tsm' ), '<span>' . get_search_query() . '</span>' );
					?>
			</h3>
			<?php
			if ( have_posts() ) : ?>
				<ul class="noticias list">
					

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile; ?>


				</ul>
				<?php
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>


<?php
get_footer();
