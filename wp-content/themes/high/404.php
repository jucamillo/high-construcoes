<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package high
 */

get_header();
?>

<section id="title-page">
	<div class="container not-search">
		<div class="col-xs-12 col-lg-8 col-md-7">
			<h4>Erro 404</h4>
			<h1>Oops, página não encontrada!</h1>
		</div>
		<div class="col-lg-4 col-md-5 col-xs-12 search-title">
			<?php get_search_form(); ?>
		</div>
	</div>
</section>

<section class="miolo list-blog-section">
	<div class="container">
		<div class="col-xs-12 search-mobile">
			
			<?php get_search_form(); ?>
		</div>
		<div class="col-xs-12">
			<?php
				get_template_part( 'template-parts/content', 'none' );
			?>

		</div>
	</div>
</section>

<?php
get_footer();
