<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package high
 */

get_header();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>



		<?php
		while ( have_posts() ) :
			the_post(); ?>


<section id="title-page">
	<div class="container">
		<div class="col-xs-12">
			<h4>Empreendimento</h4>
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</section>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		the_content();
		?>
	
</section>

<script type="text/javascript">
	
    jQuery('.vc_btn3-container > .vc_btn3-color-warning').append('<svg width="23" height="26" viewBox="0 0 23 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.75 18V14.7143M2.75 14.7143V11.2857H4.5C5.4665 11.2857 6.25 12.0532 6.25 13C6.25 13.9468 5.4665 14.7143 4.5 14.7143H2.75ZM16.75 11.4286V18M16.75 11.2857H21.125M16.75 14.7143H19.375M1 7.71429V2.71429C1 1.76751 1.7835 1 2.75 1H16.75L22 6.14286V7.71429M1 20.7143V23.2857C1 24.2325 1.7835 25 2.75 25H20.25C21.2165 25 22 24.2325 22 23.2857V20.7143M9.75 11.2857V18.1429H11.5C12.4665 18.1429 13.25 17.3753 13.25 16.4286V13C13.25 12.0532 12.4665 11.2857 11.5 11.2857H9.75Z" stroke="#F9F9F9" stroke-linecap="round" stroke-linejoin="round"/></svg>');
	
    jQuery('.vc_btn3-container > .vc_btn3-color-orange').append('<svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17 1L9 9L1 0.999999" stroke="#F9F9F9"/></svg>');

    

	jQuery('.owl-full').owlCarousel({
	    margin:0,
	    responsiveClass:true,
	    dots: false,
	    nav:true,
	    autoHeight:false,
	    autoplay: false,
	    autoplayTimeout: 10000,
	    navText: ['<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 17L1 9L9 1" stroke="#F9F9F9"/></svg>', '<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>'],
	    responsive:{
	        0:{
	            items:1,
			    dots: true,
			    nav:false,
	        },
	        768:{
	            items:2,
	            margin:1
	        },
	        992:{
	            items:1
	        },
	        1200:{
	            items:1
	        }
	    }
	})

	jQuery('.owl-video').owlCarousel({
	    margin:0,
	    responsiveClass:true,
	    dots: false,
	    nav:true,
	    autoHeight:false,
	    autoplay: false,
	    autoplayTimeout: 10000,
	    navText: ['<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 17L1 9L9 1" stroke="#F9F9F9"/></svg>', '<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>'],
	    responsive:{
	        0:{
	            items:1,
			    dots: true,
			    nav:false,
	        },
	        768:{
	            items:1
	        },
	        992:{
	            items:1
	        },
	        1200:{
	            items:1
	        }
	    }
	})

	jQuery('.owl-galery2').owlCarousel({
	    margin:0,
	    responsiveClass:true,
	    dots: false,
	    nav:true,
	    autoHeight:false,
	    autoplay: false,
	    autoplayTimeout: 10000,
	    navText: ['<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 17L1 9L9 1" stroke="#F9F9F9"/></svg>', '<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>'],
	    responsive:{
	        0:{
	            items:1,
			    dots: true,
			    nav:false,
	        },
	        768:{
	            items:2,
	            margin: 1
	        },
	        992:{
	            items:2,
	            margin: 1
	        },
	        1200:{
	            items:2,
	            margin: 1
	        }
	    }
	})
</script>


<?php
endwhile;
get_footer();
