<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package high
 */

get_header();
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>



		<?php
		while ( have_posts() ) :
			the_post(); ?>


<section id="title-page">
	<div class="container">
		<div class="col-xs-12">
			<h4>NOTÍCIAS</h4>
			<h1><?php the_title(); ?></h1>
			<div class="share">
				<h4>
					Compartilhe:
				</h4>
			<?php echo do_shortcode('[addtoany]'); ?>
			</div>
		</div>
	</div>
</section>
<section class="miolo single-blog-sec">
	<div class="container">
		<div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">

						<?php
                            echo '<ul class="blog-categories">';
                                $categories = wp_get_post_categories( get_the_ID() );
                                //loop through them
                                foreach($categories as $c){
                                  $cat = get_category( $c );
                                  //get the name of the category
                                  $cat_id = get_cat_ID( $cat->name ); 
                                  if($cat->slug != 'destaque'){
                                    //make a list item containing a link to the category
                                    echo '<li><h4><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></h4></li>';                                    
                                  }
                                }
                    		echo '</ul>';


						high_post_thumbnail();
						echo '<article>';
						the_content( );
						echo '</article> ';

			 			?>
			<div class="share">
				<h4>
					Compartilhe:
				</h4>
			<?php echo do_shortcode('[addtoany]'); ?>
			</div>

		</div>
		
		<div class="col-xs-12">
			<div class="read">
				<h3>Últimas notícias</h3>
				<?php echo do_shortcode( '[my_related_posts]' ); ?>
				
			</div>
			
		</div>
	</div>

</section>
<script type="text/javascript">

jQuery('.read .noticias').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 10000,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2,
            margin: 20
        },
        992:{
            items:2,
            margin: 20
        },
        1200:{
            items:3,
            margin: 30
        }
    }
})

jQuery('section.miolo.single-blog-sec article .gallery').addClass('owl-carousel');
jQuery('section.miolo.single-blog-sec article .gallery').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: false,
    nav:true,
    autoHeight:false,
    autoplay: false,
    autoplayTimeout: 10000,
    navText: ['<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9 17L1 9L9 1" stroke="#F9F9F9"/></svg>', '<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L9 9L1 17" stroke="#F9F9F9"/></svg>'],
    responsive:{
        0:{
            items:1,
		    dots: true,
		    nav:false,
        },
        768:{
            items:2,
            margin: 1
        },
        992:{
            items:2,
            margin: 1
        },
        1200:{
            items:2,
            margin: 1
        }
    }
})
	

jQuery('.gallery-item a').fancybox({
    selector : '.gallery-item a'
    // Options will go here
});
</script>
<?php
endwhile;
get_footer();
