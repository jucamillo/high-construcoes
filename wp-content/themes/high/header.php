<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package high
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/6c10f0500f.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://unpkg.com/scrollreveal@4"></script>



		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>
		



        <meta name="description" content="Apartamentos e imóveis de alto padrão com credibilidade, conforto e segurança. A High Construções é referência de profissionalismo. Fale com nossos corretores!">
        <meta name="robots" content="all">
        <meta name="author" content="Qualitare ">
        <meta name="keywords" content="construtora, apartamentos, joão pessoa, paraíba, empreendimento, credibilidade, alto padrão, entrega no prazo, corretor">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Pular para o conteúdo', 'high' ); ?></a>

	<header class="site-header">
		<div class="container">
			<div class="col-xs-12">
				<div class="lf">
					<div class="branding">
						<?php
						the_custom_logo();
						?>
					</div>
                    <nav id="site-navigation" class="main-navigation">
						<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
								<svg width="24" height="14" viewBox="0 0 24 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M0 1H24" stroke="#393939" stroke-width="2" stroke-linejoin="round"/>
								<path d="M8 7H24" stroke="#393939" stroke-width="2" stroke-linejoin="round"/>
								<path d="M0 13H24" stroke="#393939" stroke-width="2" stroke-linejoin="round"/>
								</svg>
						</button>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
						?>
					</nav>
				</div>
                <?php if( have_rows('chamada_para_chat', 'option') ): ?>
                <?php while( have_rows('chamada_para_chat', 'option') ): the_row(); 
                	$chamada_para_chat = get_sub_field('chamada'); ?>
                	<a  class="btn" class="chamada_para_chat" href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('nome'); ?>" >
                		<strong>
                			<?php the_sub_field('chamada'); ?>
                		</strong>
                		<span>
                			<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 1L9 9L1 17" stroke="#F9F9F9"/>
							</svg>
                			<?php the_sub_field('nome'); ?>
                		</span>
                    </a>
                <?php endwhile; ?>
                <?php endif; ?>
			</div>
		</div>
		<div class="menu-mobile">
			<div class="container">
				<div class="col-xs-12">
					<div class="top">
						<button class="menu-toggle">
							<svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1.51471 18L18.4853 1.02947" stroke="#393939" stroke-width="2" stroke-linejoin="round"/>
							<path d="M1.51474 1L18.4853 17.9706" stroke="#393939" stroke-width="2" stroke-linejoin="round"/>
							</svg>
					</button>
					</div>
				
				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1'
				) );
				?>

                <?php if( have_rows('redes_sociais', 'option') ): ?>
                    <ul class="social">  
                    <?php while( have_rows('redes_sociais', 'option') ): the_row(); ?>
                        <li class="social">
                            <a href="<?php the_sub_field('link'); ?>" target="_blank">
                                <?php the_sub_field('icone'); ?>
                            </a>
                        </li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>

                <?php if( have_rows('chamada_para_chat', 'option') ): ?>
                <?php while( have_rows('chamada_para_chat', 'option') ): the_row(); 
                	$chamada_para_chat = get_sub_field('chamada'); ?>
                	<a  class="btn" class="chamada_para_chat" href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('nome'); ?>" >
                		<span>
                			<svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 1L9 9L1 17" stroke="#F9F9F9"/>
							</svg>
                			<?php the_sub_field('nome'); ?>
                		</span>
                    </a>
                <?php endwhile; ?>
                <?php endif; ?>
				</div>
				
			</div>
		</div>
	</header><!-- #masthead -->


	<section id="content" class="miolo-site">
		
	
	<?php if ( is_front_page() ) : ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script> -->
	<div class="anima-v1">
		<div class="anima esquerda esq-anima" id="esq-anima">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l4.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />

		</div>
		<div id="animaesq-trigger"></div>
		<div class="anima direita dir-anima" id="dir-anima">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
		</div>
		<div id="animadir-trigger"></div>
	</div>





	<div class="anima-v2">
		<div class="anima esquerda esq-anima2" id="esq-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima " />

		</div>
		<div id="animaesq-trigger2"></div>
		<div class="anima direita dir-anima2" id="dir-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
		</div>
		<div id="animadir-trigger2"></div>
	</div>


	<script>
		var controller = new ScrollMagic.Controller();
			var revealElements = document.getElementsByClassName("ico-anima");
			for (var i=0; i<revealElements.length; i++) { 
				new ScrollMagic.Scene({
					triggerElement: revealElements[i], 
					offset: -50,												 
					triggerHook:1.05,
				})
				.setClassToggle(revealElements[i], "visible") 
				//.addIndicators({name: "digit " + (i+1) }) 
				.addTo(controller);
			}

			new ScrollMagic.Scene({
				triggerElement: "#animaesq-trigger", 
				offset: 0,												 
				triggerHook:.8,
			})
			.setClassToggle("#esq-anima", "visible") 
			//.addIndicators({name: "esqanima" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animadir-trigger", 
				offset: 0,												 
				triggerHook:.5,
			})
			.setClassToggle("#dir-anima", "visible") 
			//.addIndicators({name: "diranima2" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animaesq-trigger2", 
				offset: 0,												 
				triggerHook:.5,
			})
			.setClassToggle("#esq-anima2", "visible") 
			//.addIndicators({name: "esqanima2" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animadir-trigger2", 
				offset: 0,												 
				triggerHook:.8,
			})
			.setClassToggle("#dir-anima2", "visible") 
			//.addIndicators({name: "diranima" }) 
			.addTo(controller);
	</script>

	 	<?php echo do_shortcode( '[destaque_empreendimento]' ); ?>
	<?php endif; ?>










	<?php if ( is_page(12) ) : ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<!--<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script> -->
	<div class="anima-v1 revert">
		<div class="anima esquerda esq-anima" id="esq-anima">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-esq-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-esq-anima" />

		</div>
		<div id="animaesq-trigger"></div>
		<div class="anima direita dir-anima" id="dir-anima">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
		</div>
		<div id="animadir-trigger"></div>
	</div>





	<div class="anima-v2 revert">
		<div class="anima esquerda esq-anima2" id="esq-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima " />

		</div>
		<div id="animaesq-trigger2"></div>
		<div class="anima direita dir-anima2" id="dir-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
		</div>
		<div id="animadir-trigger2"></div>
	</div>


	<script>
		var controller = new ScrollMagic.Controller();
			var revealElements = document.getElementsByClassName("ico-anima");
			for (var i=0; i<revealElements.length; i++) { 
				new ScrollMagic.Scene({
					triggerElement: revealElements[i], 
					offset: -50,												 
					triggerHook:1,
				})
				.setClassToggle(revealElements[i], "visible") 
				//.addIndicators({name: "digit " + (i+1) }) 
				.addTo(controller);
			}

			new ScrollMagic.Scene({
				triggerElement: "#animaesq-trigger", 
				offset: 0,												 
				triggerHook:.4,
			})
			.setClassToggle("#esq-anima", "visible") 
			//.addIndicators({name: "esqanima" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animadir-trigger", 
				offset: 0,												 
				triggerHook:.4,
			})
			.setClassToggle("#dir-anima", "visible") 
			//.addIndicators({name: "diranima" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animaesq-trigger2", 
				offset: 0,												 
				triggerHook:.4,
			})
			.setClassToggle("#esq-anima2", "visible") 
			//.addIndicators({name: "esqanima2" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animadir-trigger2", 
				offset: 0,												 
				triggerHook:.4,
			})
			.setClassToggle("#dir-anima2", "visible") 
			//.addIndicators({name: "diranima" }) 
			.addTo(controller);
	</script>

	<?php endif; ?>






	<?php if ( is_page(14) ) : ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script> -->




	<div class="anima-v2">
		<div class="anima esquerda esq-anima2" id="esq-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima " />

		</div>
		<div id="animaesq-trigger2"></div>
		<div class="anima direita dir-anima2" id="dir-anima2">
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l1.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l3.svg" class="ico-anima ico-dir-anima" />
			<img src="<?php echo get_template_directory_uri(); ?>/images/l2.svg" class="ico-anima ico-dir-anima" />
		</div>
		<div id="animadir-trigger2"></div>
	</div>


	<script>
		var controller = new ScrollMagic.Controller();
			var revealElements = document.getElementsByClassName("ico-anima");
			for (var i=0; i<revealElements.length; i++) { 
				new ScrollMagic.Scene({
					triggerElement: revealElements[i], 
					offset: -50,												 
					triggerHook:1.05,
				})
				.setClassToggle(revealElements[i], "visible") 
				//.addIndicators({name: "digit " + (i+1) }) 
				.addTo(controller);
			}


			new ScrollMagic.Scene({
				triggerElement: "#animaesq-trigger2", 
				offset: 0,												 
				triggerHook:.5,
			})
			.setClassToggle("#esq-anima2", "visible") 
			//.addIndicators({name: "esqanima2" }) 
			.addTo(controller);


			new ScrollMagic.Scene({
				triggerElement: "#animadir-trigger2", 
				offset: 0,												 
				triggerHook:.8,
			})
			.setClassToggle("#dir-anima2", "visible") 
			//.addIndicators({name: "diranima" }) 
			.addTo(controller);
	</script>

	<?php endif; ?>



