<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                echo '<li class="single">
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');"></a>

                                <div class="info">';

                                    $terms = get_the_terms( $post->ID, 'categoria' );
                                        if($terms){
                                           echo '<h4>';
                                            foreach ($terms as $term) {
                                                if($term->slug != 'destaque'){
                                                echo '<a href="'.get_home_url().'/empreendimentos/categoria/'.$term->slug.'" title="'.$term->name.'">'.$term->name.'</a>';

                                                }
                                            }
                                            echo '</h4>';
                                        }
                                    echo '<h3><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>
                                                <div class="btn-full">
                                                    <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="btn">
                                                    <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1L9 9L1 17" stroke="#ED292E"/>
                                                    </svg>
                                                    <span>
                                                    Ver empreendimento
                                                    </span>
                                                    </a>
                                                </div>
                                        </div>
                                   	</li>'; ?>